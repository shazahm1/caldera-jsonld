<?php


namespace calderaultra\jsonld;
use calderaultra\jsonld\Contexts\Blog;
use calderaultra\jsonld\Contexts\EDD;
use calderaultra\jsonld\Contexts\Implementation;
use calderaultra\jsonld\Contexts\Page;


/**
 * Class Init
 * @package calderaultra\jsonld
 */
class Init {

	/**
	 * @var Yoast
	 */
	protected  static $yoast;

	/**
	 * Maybe print a script tag
	 *
	 * @since 0.0.1
	 */
	public static function maybe_print(){
		$script = self::maybe_create();
		if( $script  ){
			echo $script;
		}

	}

	/**
	 * Based on context of current request maybe create the script tag
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public static function maybe_create(){
		$object_id = get_queried_object_id();

		/**
		 * Early access point for LD+JSON generator
		 *
		 * @since 0.1.1
		 *
		 * Return an object implimenting \calderaultra\jsonld\Contexts\Implementation interface to use that to generate markup. Return null to use default logic. Return a different truthy value to prevent anything from being generatred.
		 *
		 * @param \calderaultra\jsonld\Contexts\Implementation|null $creator Creator object
		 * @param int|null $object_id Current object ID. Found with get_queried_object_id()
		 */
		$creator = apply_filters( 'calderaultra_ldjson_creator', null, $object_id );

		if (  ! $creator ) {
			$postTypes = self::getSupportedPostTypes();
			if ( is_singular( 'download' ) && class_exists( 'EDD_Download' ) ) {
				$creator = new EDD( new \EDD_Download( $object_id ) );

			} elseif ( is_singular( $postTypes ) ) {
				$creator = new Blog( get_post( $object_id ) );
			} elseif ( is_page() && in_array( 'page', $postTypes ) ) {
				$creator = new Page( get_post( $object_id ) );
			}
		}


		if( is_a( $creator,'\calderaultra\jsonld\Contexts\Implementation'  ) ){
			return $creator->create();
		}
	}

	/**
	 * Get supported post types
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public static function getSupportedPostTypes()
	{
		return apply_filters( 'calderaultra_ldjson_supported_post_types', [
			'post',
			'page'
		]);
	}

	/**
	 * Add hooks to use Yoast Data when possible
	 *
	 * @since 0.0.1
	 */
	public static function addYoastHooks()
	{
		if( class_exists( 'WPSEO_Meta' ) ){
			if( is_singular() ) {
				self::$yoast = new \calderaultra\jsonld\Yoast( (int) get_queried_object_id() );
				add_filter( 'calderaultra_ldjson_context_args', [ self::$yoast, 'useMetaDescription' ] );
				add_filter( 'calderaultra_ldjson_context_args', [ self::$yoast, 'prependFocusKeyword' ] );
				add_filter( 'calderaultra_ldjson_context_args', [ self::$yoast, 'changeTitle' ] );
			}

		}
	}

	/**
	 * Get Yoast SEO integration object
	 *
	 * @since 0.0.1
	 *
	 * @return Yoast
	 */
	public static function getYoast()
	{
		return self::$yoast;
	}



}