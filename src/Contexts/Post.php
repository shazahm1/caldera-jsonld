<?php


namespace calderaultra\jsonld\Contexts;
use calderaultra\jsonld\Helpers;
use JsonLd\Context;


/**
 * Class Post
 * @package calderaultra\jsonld\Contexts
 */
abstract class Post implements Implementation {

	/** @var \WP_Post  */
	protected $post;

	/**
	 * @var Context
	 */
	protected $featured;

	/**
	 * @var Context
	 */
	protected $author;

	/**
	 * Post constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param \WP_Post $post
	 */
	public function __construct( \WP_Post $post )
	{
		$this->post = $post;
	}

	/**
	 * Create script tag
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function create()
	{

		$properties = $this->generateProperties();
		$filter = CreateContext::filterName( $this->type(), $this->postType() );
		if( ! empty( $properties[ 'image' ] ) ){
			add_filter( $filter, function( $properties ){
				$properties[ 'image' ] = $this->featured->getProperties();
				return $properties;
			});
		}

		if( ! empty( $properties[ 'author' ] ) ){
			add_filter( $filter, function( $properties ){
				$properties[ 'author' ] = $this->author->getProperties();
				return $properties;
			});
		}

		$properties[ '@type' ] = $this->type();

		return CreateContext::render( $this->type(), $properties, $this->postType() );
	}

	/**
	 * @inheritdoc
	 * @since 0.0.1
	 */
	public function generateProperties() : array
	{
		$props = [
			'name' => get_the_title( $this->post ),
			'url' => get_permalink( $this->post ),
			'description' => get_the_excerpt( $this->post ),
			'image' => $this->featuredImage( $this->post->ID ),
			'video' => null,
			'thumbnailUrl' => get_the_post_thumbnail_url( $this->post->ID ),
			'review' => null,
			'publisher' => null,
			'dateCreated' => get_the_date( '',  $this->post ),
			'dateModified' => get_the_modified_date( '', $this->post ),
			'datePublished' => get_the_date( '',  $this->post ),
			'author' => $this->author( $this->post->post_author ),
			'aggregateRating' => null,
			'wordCount' => str_word_count( $this->post->post_content ),
			'articleBody' => $this->post->post_content,
			'articleSection' => null,
			'pageEnd' => null,
			'pageStart' => null,
			'pagination' => null,
			'mainEntityOfPage' => null,
			'headline' => get_the_title( $this->post ),
			'keywords' => $this->keywords()
		];


		return $props;

	}

	/**
	 * Get categories and tags for keywords
	 *
	 * @return string
	 */
	public function keywords(){

		$cats = get_the_category( $this->post->ID );
		if( is_array( $cats ) && ! empty( $cats ) ){
			$cats = wp_list_pluck( $cats, 'name' );
		}else{
			$cats = [];
		}

		$tags = get_the_tags(   $this->post->ID );
		if( is_array( $tags ) && ! empty( $tags ) ){
			$tags = wp_list_pluck( $tags, 'name' );
		}else{
			$tags = [];
		}

		$list = array_merge( $cats, $tags );


		return implode( ' ',  $list );
	}

	/**
	 * Get featured image as context object
	 *
	 * @param $post_id
	 *
	 * @return Context
	 */
	protected function featuredImage( $post_id )
	{
		$this->featured = $this->image( get_post_thumbnail_id( $post_id ) );
		return $this->featured;
	}

	/**
	 * Get post type
	 *
	 * @since 0.0.1
	 *
	 * @return false|string
	 */
	protected function postType()
	{
		return get_post_type( $this->post );
	}


	/**
	 * Get type for filter name
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	protected function type(){
		return 'BlogPosting';
	}

	/**
	 * Create context object for an image
	 *
	 * @since 0.0.1
	 *
	 * @param int $id Image ID
	 * @param string $size Size to use
	 * @param bool $addThumb Optional. Add child object for thumbnail?
	 *
	 * @return Context
	 */
	protected function image( $id, $size = 'large', $addThumb = true )
	{
		return Helpers::image( $id, $size, $addThumb );

	}

	/**
	 * Get author as context object
	 *
	 * @param int $userId Author ID
	 *
	 * @return Context
	 */
	protected function author( $userId )
	{

		$this->author = Helpers::author( $userId );
		return $this->author;

	}
}