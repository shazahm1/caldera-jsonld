<?php


namespace calderaultra\jsonld;
use calderaultra\jsonld\Contexts\CreateContext;
use JsonLd\Context;


/**
 * Class Helpers
 * @package calderaultra\jsonld
 */
class Helpers {

	/**
	 * Create context object for an image
	 *
	 * @since 0.0.1
	 *
	 * @param int $id Image ID
	 * @param string $size Size to use
	 * @param bool $addThumb Optional. Add child object for thumbnail?
	 *
	 * @return Context
	 */
	public static function image( $id, $size = 'large', $addThumb = true )
	{

		$img = wp_get_attachment_image_src( $id, $size );
		if( ! empty( $img ) ){

			$props = [
				'url' => $img[0],
				'height' => $img[1],
				'width' => $img[2],
				'caption' => get_post_meta( $id, '_wp_attachment_image_alt', true ),
				'thumbnail' => null
			];

			if( $addThumb ){
				$props[ 'thumbnail' ] = self::image( $id, 'thumbnail', false );
			}

			return  CreateContext::create( 'ImageObject', $props, 'image' );

		}

	}

	/**
	 * Get author as context object
	 *
	 * @param int $userId Author ID
	 *
	 * @return Context
	 */
	public static  function author( $userId ){
		$user = get_user_by( 'ID', $userId );
		$props = [
			'name' => $user->display_name,
			'url' => $user->user_url,
			'description' => $user->description,
			'image' => get_avatar_url( $userId ),
		];

		return CreateContext::create( 'Person', $props, 'author' );
	}

}