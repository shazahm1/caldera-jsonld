<?php
use \calderaultra\jsonld\Init;
/**
 * Plugin Name: Caldera Ultra JSON-ld
 * Version: 0.1.2
 */
/**
 * Copyright 2017 CalderaWP LLC
 * Licensed under the terms of the GNU GPL v2 or later.
 */

/**
 * Make plugin go
 *
 * @since 0.0.1
 */
add_action( 'plugins_loaded', function(){
	include_once  __DIR__ . '/vendor/autoload.php';
	add_action( 'wp_head', 'calderaultra_jsonld_header' );
	add_action( 'template_redirect', function(){
		Init::addYoastHooks();
	});

},0);

/**
 * When possible print script in header
 *
 * @uses "wp_head" action
 *
 * @since 0.0.1
 */
function calderaultra_jsonld_header(){
	Init::maybe_print();
}


